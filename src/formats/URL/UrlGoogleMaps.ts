import { flow, pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/lib/Array';
import {
  generateMakeParseCoordinates,
  makeCoordinatesFormat,
  makeCoordinatesParserError,
  makeCoordinatesParserSuccess,
} from '../../constructors';
import { formatUrlGoogleMaps } from './constants';
import { LatLonTuple } from '../../types';
import { matchesRegex } from '../../utils/regex';
import { split } from 'fp-ts/lib/string';
import { DD } from '../WGS84';

export const urlGoogleMapsToLatLon = (str: string) =>
  pipe(
    str,
    split('/'),
    (x) => x.find((a) => a.startsWith('@')),
    (x) => x?.replace('@', '') || '',
    split(','),
    (x) => [x[0], x[1]] as [string, string],
    A.map((x) => parseFloat(x)),
    (x) => x as LatLonTuple
  );

export type CoordinatesGoogleMaps = LatLonTuple;

export const latLonToUrlGoogleMaps = (latLon: LatLonTuple) =>
  `https://www.google.com/maps/@${latLon[0]},${latLon[1]}`;

export const validateUrlGoogleMaps = (str: string) =>
  pipe(
    '.*google\\.com.+\\/@-?\\d+\\.\\d+,-?\\d+\\.\\d+.*',
    matchesRegex(str),
    (isValid) => (isValid ? [] : ['Could not parse Google Maps Url.'])
  );

export const parseUrlGoogleMaps = generateMakeParseCoordinates(
  () => (str: string) =>
    pipe(
      str,
      validateUrlGoogleMaps,
      (errorMessages) =>
        errorMessages.length
          ? makeCoordinatesParserError({
              format: formatUrlGoogleMaps,
              meta: {
                str,
              },
              errors: errorMessages,
            })
          : makeCoordinatesParserSuccess({
              format: formatUrlGoogleMaps,
              latLon: urlGoogleMapsToLatLon(str),
              meta: {},
            }),
      (x) => [x]
    )
);

export const UrlGoogleMaps = makeCoordinatesFormat({
  format: formatUrlGoogleMaps,
  coordinatesToLatLon: urlGoogleMapsToLatLon,
  latLonToCoordinates: latLonToUrlGoogleMaps,
  defaultStringifyOptions: {},
  makeCoordinatesToString: () => flow(urlGoogleMapsToLatLon, DD.latLonToString),
  validateCoordinates: validateUrlGoogleMaps,
  defaultParserOptions: {},
  makeParseCoordinates: parseUrlGoogleMaps,
});
