import { pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/lib/Array';
import * as R from 'fp-ts/lib/Record';
import {
  makeCoordinatesToLatLon,
  makeLatLonToCoordinates,
} from '../../../constructors';
import {
  decimalToHexagecimal,
  hexagecimalToDecimal,
  negateIf,
  numberToString,
  parseIntDecimal,
} from '../../../utils/numbers';
import { LatLonTuple } from '../../../types';

export const decimalToCoordinateDM = (x: number) =>
  pipe(x, numberToString, parseIntDecimal, (int) => ({
    degrees: int,
    minutes: decimalToHexagecimal(x - int),
  }));

export const latLonToCoordinatesDM = makeLatLonToCoordinates((x) =>
  pipe(
    {
      latitude: x[0],
      longitude: x[1],
    },
    R.map(decimalToCoordinateDM)
  )
);

export type CoordinatesDM = ReturnType<typeof latLonToCoordinatesDM>;

export const coordinatesToLatLonDM = makeCoordinatesToLatLon(
  ({ latitude, longitude }: CoordinatesDM) =>
    pipe(
      [latitude, longitude],
      A.map(({ degrees, minutes }) =>
        pipe(
          Math.abs(degrees) + hexagecimalToDecimal(minutes),
          negateIf(degrees < 0)
        )
      ),
      (x) => x as LatLonTuple
    )
);
