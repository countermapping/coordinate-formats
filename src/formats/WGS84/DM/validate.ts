import { pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/lib/Array';
import * as O from 'fp-ts/lib/Option';
import { makeValidateCoordinates } from '../../../constructors';
import { CoordinatesDM } from './convert';
import { validateDegrees, validateMinutes } from '../validations';
import { CoordinatePartsDM, ParserOptionsDM } from './parse';
import {
  containCharactersConsistently,
  containsOnlyValidCharacters,
} from '../../../validations';

export const validateCoordinatePartsDM = (
  parts: CoordinatePartsDM,
  options: ParserOptionsDM
) =>
  pipe(
    [
      containsOnlyValidCharacters({
        key: 'prefix',
        str: parts.prefix,
        validCharacters: [
          ...options.symbols.north,
          ...options.symbols.south,
          ...options.symbols.negative,
          ...options.symbols.whitespace,
        ],
      }),
      containsOnlyValidCharacters({
        key: 'latitude (between degrees and minutes)',
        str: parts.latBetweenDegAndMin,
        validCharacters: [
          ...options.symbols.degrees,
          ...options.symbols.whitespace,
          ...options.separators.number,
        ],
      }),
      containsOnlyValidCharacters({
        key: 'middle',
        str: parts.middle,
        validCharacters: [
          ...options.symbols.minutes,
          ...options.symbols.east,
          ...options.symbols.west,
          ...options.symbols.north,
          ...options.symbols.south,
          ...options.symbols.negative,
          ...options.symbols.whitespace,
          ...options.separators.coordinates,
        ],
      }),
      containsOnlyValidCharacters({
        key: 'longitude (between degrees and minutes)',
        str: parts.lonBetweenDegAndMin,
        validCharacters: [
          ...options.symbols.degrees,
          ...options.symbols.whitespace,
          ...options.separators.number,
        ],
      }),
      containsOnlyValidCharacters({
        key: 'suffix',
        str: parts.suffix,
        validCharacters: [
          ...options.symbols.east,
          ...options.symbols.west,
          ...options.symbols.minutes,
          ...options.symbols.whitespace,
        ],
      }),
      containCharactersConsistently({
        targets: [parts.latDegrees, parts.lonDegrees],
        characters: [
          ...options.separators.decimal,
          ...options.symbols.whitespace,
        ],
      }),
      containCharactersConsistently({
        targets: [parts.latBetweenDegAndMin, parts.lonBetweenDegAndMin],
        characters: [
          ...options.symbols.degrees,
          ...options.separators.number,
          ...options.symbols.whitespace,
        ],
      }),
      containCharactersConsistently({
        targets: [parts.latMinutesBetween, parts.lonMinutesBetween],
        characters: [
          ...options.separators.decimal,
          ...options.symbols.whitespace,
        ],
      }),
    ],
    A.filterMap(O.fromNullable)
  );

export const validateCoordinatesDM = makeValidateCoordinates(
  ({ latitude, longitude }: CoordinatesDM) =>
    pipe(
      [
        { key: 'latitude', coordinate: latitude },
        { key: 'longitude', coordinate: longitude },
      ],
      A.chain(({ key, coordinate }) => [
        validateDegrees({ key, degrees: coordinate.degrees }),
        validateMinutes({ key, minutes: coordinate.minutes }),
      ]),
      A.filterMap(O.fromNullable)
    )
);
