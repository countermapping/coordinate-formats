import {
  BACKSLASH_CONFUSABLES,
  COLON_CONFUSABLES,
  COMMA_CONFUSABLES,
  DEGREE_SIGN_CONFUSABLES,
  DOT_CONFUSABLES,
  DOUBLE_QUOTE_CONFUSABLES,
  MINUS_CONFUSABLES,
  SEMICOLON_CONFUSABLES,
  SINGLE_QUOTE_CONFUSABLES,
  SLASH_CONFUSABLES,
  WHITESPACE_CONFUSABLES,
} from '../../constants';

/**
 * @category Default
 */
export const defaultCharactersets = {
  symbols: {
    north: ['N', 'n', 'north'],
    south: ['S', 's', 'south'],
    east: ['E', 'e', 'east'],
    west: ['W', 'w', 'west'],
    negative: MINUS_CONFUSABLES,
    degrees: [
      ...DEGREE_SIGN_CONFUSABLES,
      'deg',
      'd',
      'D',
      'Deg',
      'degrees',
      'Degrees',
    ],
    minutes: [
      ...SINGLE_QUOTE_CONFUSABLES,
      'm',
      'M',
      'Min',
      'min',
      'minutes',
      'Minutes',
    ],
    seconds: [
      ...DOUBLE_QUOTE_CONFUSABLES,
      's',
      'S',
      'sec',
      'Sec',
      'seconds',
      'Seconds',
    ],
    whitespace: WHITESPACE_CONFUSABLES,
  },
  separators: {
    number: [...COLON_CONFUSABLES, ...DOT_CONFUSABLES, ...COMMA_CONFUSABLES],
    decimal: [
      ...DOT_CONFUSABLES,
      ...COMMA_CONFUSABLES,
      ...WHITESPACE_CONFUSABLES,
    ],
    coordinates: [
      ...COMMA_CONFUSABLES,
      ...BACKSLASH_CONFUSABLES,
      ...SLASH_CONFUSABLES,
      ...SEMICOLON_CONFUSABLES,
    ],
  },
};
