import { flow, pipe } from 'fp-ts/lib/function';
import * as O from 'fp-ts/lib/Option';
import { not } from 'fp-ts/lib/Predicate';
import { formatSentence } from '../utils/strings';

/**
 * @category Constructor
 */
export const makeValidation =
  <A extends Record<string, unknown>>(
    predicate: (args: A) => boolean,
    makeMessage: (args: A) => string
  ) =>
  (args: A) =>
    pipe(
      args,
      O.fromPredicate(not(predicate)),
      O.fold(() => undefined, flow(makeMessage, formatSentence))
    );
