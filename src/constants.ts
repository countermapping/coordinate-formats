/**
 * @category Constant
 * @see [Unicode Utilities](https://util.unicode.org/UnicodeJsps/confusables.jsp?a=-&r=None)
 */
export const MINUS_CONFUSABLES = [
  '‐',
  '‑',
  '‒',
  '−',
  '–',
  '⁃',
  '۔',
  '➖',
  '˗',
  '﹘',
  'Ⲻ',
  '-',
  `—`,
];

/**
 * @category Constant
 * @see [Unicode Utilities](https://util.unicode.org/UnicodeJsps/confusables.jsp?a=%C2%B0&r=None)
 */
export const DEGREE_SIGN_CONFUSABLES = ['°', '⸰', '◦', '∘', '˚', '○', 'º', '*'];

/**
 * @category Constant
 * @see [Unicode Utilities](https://util.unicode.org/UnicodeJsps/confusables.jsp?a=%27&r=None)
 */
export const SINGLE_QUOTE_CONFUSABLES = [
  `｀`,
  `΄`,
  `＇`,
  `ˈ`,
  `ˊ`,
  `ᑊ`,
  `ˋ`,
  `ꞌ`,
  `ᛌ`,
  `𖽒`,
  `𖽑`,
  `‘`,
  `’`,
  `י`,
  `՚`,
  `‛`,
  `՝`,
  '`',
  `'`,
  `′`,
  `׳`,
  `´`,
  `ʹ`,
  `˴`,
  `ߴ`,
  `‵`,
  `ߵ`,
  `ʻ`,
  `ʼ`,
  `᾽`,
  `ʽ`,
  `῾`,
  `ʾ`,
  `᾿`,
];

/**
 * @category Constant
 * @see [Unicode Utilities](https://util.unicode.org/UnicodeJsps/confusables.jsp?a=%22&r=None)
 */
export const DOUBLE_QUOTE_CONFUSABLES = [
  '"',
  '＂',
  '〃',
  'ˮ',
  'ײ',
  '᳓',
  '″',
  '״',
  '‶',
  '˶',
  'ʺ',
  '“',
  '”',
  '˝',
  '‟',
  ...SINGLE_QUOTE_CONFUSABLES.map((x) => `${x}${x}`),
];

/**
 * @category Constant
 * @see [Unicode Utilities](https://util.unicode.org/UnicodeJsps/confusables.jsp?a=%3A&r=None)
 */
export const COLON_CONFUSABLES = [
  'ः',
  ',ઃ',
  '܃',
  '᠃',
  '׃',
  '܄',
  '։',
  '᠉',
  '꞉',
  '᛬',
  '︰',
  'ː',
  '∶',
  '˸',
  ':',
  '：',
  '⁚',
  'ꓽ',
];

/**
 * @category Constant
 * @see [Unicode Utilities](https://util.unicode.org/UnicodeJsps/confusables.jsp?a=.&r=None)
 */
export const DOT_CONFUSABLES = [
  '٠',
  '۰',
  '܁',
  ',𝅭',
  '܂',
  '․',
  '𐩐',
  'ꓸ',
  '.',
  '꘎',
];

/**
 * @category Constant
 * @see [Unicode Utilities](https://util.unicode.org/UnicodeJsps/confusables.jsp?a=%2C&r=None)
 */
export const COMMA_CONFUSABLES = ['¸', 'ꓹ', '‚', '٫', '؍', ','];

/**
 * @category Constant
 * @see [Unicode Utilities](https://util.unicode.org/UnicodeJsps/confusables.jsp?a=%2F&r=None)
 */
export const SLASH_CONFUSABLES = [
  '⁁',
  '⼃',
  '⁄',
  'Ⳇ',
  '⟋',
  'ノ',
  '/',
  '╱',
  '㇓',
  '〳',
  '᜵',
  '∕',
  '⧸',
  '𝈺',
  '丿',
];

/**
 * @category Constant
 * @see [Unicode Utilities](https://util.unicode.org/UnicodeJsps/confusables.jsp?a=%5C&r=None)
 */
export const BACKSLASH_CONFUSABLES = [
  '𝈏',
  '⼂',
  '㇔',
  '⧵',
  '∖',
  '丶',
  '﹨',
  '⧹',
  '\\',
  '＼',
  '𝈻',
  '⟍',
];

/**
 * @category Constant
 * @see [Unicode Utilities](https://util.unicode.org/UnicodeJsps/confusables.jsp?a=+&r=None)
 */
export const WHITESPACE_CONFUSABLES = [
  ' ',
  ' ',
  ' ',
  ' ',
  ' ',
  ' ',
  ' ',
  ' ',
  ' ',
  ' ',
  ' ',
];

/**
 * @category Constant
 * @see [Unicode Utilities](https://util.unicode.org/UnicodeJsps/confusables.jsp?a=%3B&r=None)
 */
export const SEMICOLON_CONFUSABLES = [';', ';'];
