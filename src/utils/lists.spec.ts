import { join } from './lists';

describe('utils/lists', () => {
  describe('join()', () => {
    it("joins a list's elements", () => {
      const data = [1, 2, 3];
      expect(join(', ')(data)).toEqual('1, 2, 3');
    });
  });
});
