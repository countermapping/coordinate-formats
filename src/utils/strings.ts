import { flow } from 'fp-ts/lib/function';
import * as S from 'fp-ts/lib/string';
import * as T from 'fp-ts/lib/Tuple';
import { join } from './lists';
import * as A from 'fp-ts/lib/Array';
import {
  create,
  execute,
  fenceString,
  nonCapturingGroup,
  oneOrMore,
  or,
} from './regex';

/**
 * @category Utility
 */
export const split = (separator: string) => (str: string) =>
  str.split(separator);

/**
 * @category Utility
 */
export const replacecharacters = (x: string, y: string) => (str: string) =>
  str.replace(x, y);

/**
 * @category Utility
 */
export const removecharacters = (x: string) => replacecharacters(x, '');

/**
 * @category Utility
 */
export const append = (b: string) => (a: string) => `${a}${b}`;

/**
 * @category Utility
 */
export const wrap = (a: string, b: string) => (x: string) => `${a}${x}${b}`;

/**
 * @category Utility
 */
export const mapFstChar = (fn: (s: string) => string) =>
  flow(
    (str: string) => [str[0], str.slice(1)] as [string, string],
    T.mapFst(fn),
    join('')
  );

/**
 * @category Utility
 */
export const formatSentence = flow(mapFstChar(S.toUpperCase), append('.'));

export const indexOf = (substr: string) => (str: string) => str.indexOf(substr);
