import { pipe } from 'fp-ts/lib/function';
import { parseFloatFromParts, toFixedIntegerDigits } from './numbers';

describe('numbers', () => {
  describe('parseFloatFromParts()', () => {
    it('parses a float number from parts', () => {
      expect(parseFloatFromParts('20', '12')).toEqual(20.12);
      expect(parseFloatFromParts('20', '')).toEqual(20);
      expect(parseFloatFromParts('', '')).toEqual(NaN);
    });
  });
  describe('toFixedIntegerDigits()', () => {
    it('rounds to the correct number of fixed integer digits', () => {
      expect(pipe(3, toFixedIntegerDigits(2))).toEqual('03');
      expect(pipe(23.43, toFixedIntegerDigits(2))).toEqual('23.43');
      expect(pipe(23.43, toFixedIntegerDigits(4))).toEqual('0023.43');
      expect(pipe(0.5, toFixedIntegerDigits(2))).toEqual('00.5');
      expect(pipe(0, toFixedIntegerDigits(2))).toEqual('00');
    });
  });
});
