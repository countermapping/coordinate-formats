import { mapFstChar } from './strings';

describe('utils/strings', () => {
  describe(mapFstChar, () => {
    it('maps the first character using the given function', () => {
      expect(mapFstChar((x) => x.toUpperCase())('abc')).toEqual('Abc');
    });
  });
});
