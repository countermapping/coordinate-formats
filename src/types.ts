/**
 * @category Core
 */
export type LatLonTuple = [latitude: number, longitude: number];

export type ArrayElement<T> = T extends Array<infer R> ? R : never;
