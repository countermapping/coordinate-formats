import {
  containCharactersConsistently,
  containsOnlyValidCharacters,
} from './validations';

describe('validations', () => {
  describe('containsNoInvalidCharacters()', () => {
    it('works', () => {
      expect(
        containsOnlyValidCharacters({
          key: 'test',
          str: `''`,
          validCharacters: [`''`],
        })
      ).toBe(undefined);
      expect(
        containsOnlyValidCharacters({
          key: 'test',
          str: `' `,
          validCharacters: [`''`, ' '],
        })
      ).not.toBe(undefined);
    });
  });
  describe('containsCharactersConsistently', () => {
    it('detects (in-)consistency for a single character', () => {
      expect(
        containCharactersConsistently({
          targets: ['@', '@'],
          characters: ['@'],
        })
      ).toBe(undefined);
      expect(
        containCharactersConsistently({
          targets: ['@', 'at'],
          characters: ['@'],
        })
      ).not.toBe(undefined);
    });
    it('detects (in-)consistency for multiple characters', () => {
      expect(
        containCharactersConsistently({
          targets: ['@', '@'],
          characters: ['@', '.'],
        })
      ).toBe(undefined);
      expect(
        containCharactersConsistently({
          targets: ['@.', '@.'],
          characters: ['@', '.'],
        })
      ).toBe(undefined);
      expect(
        containCharactersConsistently({
          targets: ['@', '@.'],
          characters: ['@', '.'],
        })
      ).not.toBe(undefined);
      expect(
        containCharactersConsistently({
          targets: ['.', '@.'],
          characters: ['@', '.'],
        })
      ).not.toBe(undefined);
    });
  });
});
