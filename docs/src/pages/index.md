---
hide_table_of_contents: true
---

# coordinate-formats

A library for parsing, validating and stringifying geographical coordinates of various formats.

## Features

- 🤔 Parses **possible coordinates of different formats** if the received input is ambigious. By default the following formats are recognized:
    - Decimal Degrees (Lat/Lon)
    - Degrees Minutes
    - Degrees Minutes Seconds
    - Open Location Code
    - OpenStreetMap URL
    - Google Maps URL


- ⚙️ Uses **sensible defaults** i.e. for **parsing [confusable unicode characters](https://util.unicode.org/UnicodeJsps/confusables.jsp)**.

- 🗺️ Made to **integrate easily with mapping libraries** like [Leaflet.js](https://leafletjs.com/) and [Mapbox GL JS](https://www.mapbox.com/mapbox-gljs).

- 💡 Written in TypeScript for a **fully typed API**.

- 🏗️ Provides utilities and for adding **custom coordinate formats**.
